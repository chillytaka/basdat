<?php
	include("connect.php");
	if ($_SESSION["status"] != "staff") {
		header("Location: index.html");
		die();
	} else {
		$id = $_POST["idMhs"];
		$nama = $_POST["nama"];
		
		$sql = 'select *  from peserta_matkul where NRP_Mhs=?';
		$stmt = $pdo->prepare($sql);
		$stmt->execute([$id]);
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Data Mahasiswa</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<style>
	.checkbox{
		width:30px; height: 30px;
	}

	</style>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('https://www.its.ac.id/wp-content/uploads/2017/09/ini-bunderan-its-ya-1-1024x576.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-11">
					Mata Kuliah mahasiswa :
				</span>
				<span class="login100-form-title p-b-41">
					<?php echo $nama ?>
				</span>
				<div class="login100-form validate-form p-b-33 p-t-5">
					<table class="table">
  <thead class="thead-dark">
    <tr>
      <center><th scope="col">Mata kuliah</th></center>
      <center><th scope="col">Dosen</th></center>
			<center><th scope="col"></th></center>
    </tr>
  </thead>
  <tbody>
	  <?php 
	 	for ($i = 0; $row = $stmt->fetch();$i++) {
			 ?> 
				<tr>
					<th scope="row"><?php echo $row["nama_MK"] ?></th>
					<td scope="row"><?php echo $row["nama_Dosen"] ?></td>
					<td>
						<form class="container-login100-form-btn" method="POST" action="removeMK.php">
							<input type="hidden" value=<?php echo $row["ID_Diambil"] ?> name="idMk" />
							<button class="login100-form-btn">DROP</button>
						</form>
					</td>
				</tr>
			 <?php
		 }
	  ?>
  </tbody>
</table>
<div class="container-login100-form-btn">
	<a class="login100-form-btn" href="tambahMk.php">TAMBAH MATKUL</a>
</div>
				</div>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
