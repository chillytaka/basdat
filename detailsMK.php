<?php
	include("connect.php");
	if ($_SESSION["status"] != "staff") {
		header("Location: index.html");
		die();
	} else {
		$week = $_POST["week"];
		$mk = $_POST["mk"];
		$nama = $_POST["namaMK"];

		if (isset($week)) {
			$newSql = 'select * from peserta_matkul as `peserta` left join tb_absensi as `absen`
					on `absen`.FK_ID_Diambil = `peserta`.ID_Diambil and `absen`.minggu_Absensi =?
					where `peserta`.ID_KelasMK=?';
			$newQuery = $pdo->prepare($newSql);
			$newQuery->execute([$week,$mk]);
		}
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>kehadiran</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<style>
	.checkbox{
		width:30px; height: 30px;
	}

	</style>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('https://www.its.ac.id/wp-content/uploads/2017/09/ini-bunderan-its-ya-1-1024x576.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-11">
					Kehadiran Mahasiswa
				</span>
				<span class="login100-form-title p-b-41">
					<?php echo $nama ?>
				</span>
				<span class="login100-form-title p-b-41">
					<?php echo $week ?>
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" method="POST" action="addconfirm.php">
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">NRP</th>
								<th scope="col">Nama</th>
                                <th scope="col">status absen</th>
                                <th scope="col">verifikasi dosen</th>
							</tr>
						</thead>
						<tbody>
							<?php
								for ($b = 0; $hasil = $newQuery->fetch(); $b++) {
									?> 
									<tr>
										<input type="hidden" name="id[]" value=<?php echo $hasil["ID_Diambil"] ?> />
										<input type="hidden" name="minggu" value=<?php echo $week ?> />
										<th scope="row"><?php echo $hasil["NRP_Mhs"] ?></th>
										<td scope="row"><?php echo $hasil["nama_Mhs"] ?></td>
                                		<td scope="row">
                                            <select id="statusAbsen" class="input100" name="status[]">
												<?php 
													switch($hasil["keterangan_Absensi"]) {
														case 'hadir':
															?> 
																<option value=1 selected>Hadir</option>
                                                                <option value=2>Sakit</option>
                                                                <option value=3>Izin</option>
                                                                <option value=4>Tanpa Keterangan</option>
															<?php
														break;
														case 'sakit':
															?> 
																<option value=1>Hadir</option>
                                                                <option value=2 selected>Sakit</option>
                                                                <option value=3>Izin</option>
                                                                <option value=4>Tanpa Keterangan</option>
															<?php
														break;
														case 'izin':
															?> 
																<option value=1>Hadir</option>
                                                                <option value=2>Sakit</option>
                                                                <option value=3 selected>Izin</option>
                                                                <option value=4>Tanpa Keterangan</option>
															<?php
														break;
															default:
															?> 
																<option value=1>Hadir</option>
                                                                <option value=2>Sakit</option>
                                                                <option value=3>Izin</option>
                                                                <option value=4 selected>Tanpa Keterangan</option>
															<?php
														break;
	
													}
												?>
											</select>
										</td>
										<td scope="row">
											<?php 
												if ($hasil["verifDsn_Absensi"] == 1) {
													echo "Sudah Diverifikasi";
												} else {
													echo "Belum Diverifikasi";
												}
											?>
										</td>
									</tr>	
									<?php
								}
							?>
                        </tbody>
                    </table>
                    <div class="container-login100-form-btn m-t-32">
                        <input type="submit" class="login100-form-btn" value="Simpan Data">
					</div>
				</form>
				<form method="POST" action="logout.php">
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Logout
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
