<?php

    include('connect.php');

    $user = $_POST["username"];
    $pass = $_POST["pass"];

    $sql = 'call sp_Login(?,?,@result,@id)';

    $stmt = $pdo->prepare($sql);
    $stmt->execute([$user, $pass]);
    $stmt->closeCursor();

    $row = $pdo->query("select @result, @id")->fetch(PDO::FETCH_ASSOC);
    
    $user_result = $row['@result'];
    $id_result = $row['@id'];

    $_SESSION["status"] = $user_result;
    $_SESSION["id"] = $id_result;

    switch($user_result) {
        case "mhs":
            header("Location: berandamahasiswa.php");
            die();
            break;
        case "dosen":
            header("Location: berandadosen.php");
            die();
            break;
        case "ortu":
            header("Location: berandaortu.php");
            die();
            break;
        case "staff":
            header("Location: berandastaff.php");
            die();
            break;
        default:
            unset($_SESSION["status"],$_SESSION["id"]);
            header("Location: index.html");
            die();
    }
?>