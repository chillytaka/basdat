<?php
	include("connect.php");
	if ($_SESSION["status"] != "mhs") {
		header("Location: index.html");
		die();
	} else {
	$id = $_SESSION["id"];

	$sql = 'select * from peserta_matkul where NRP_Mhs=?';
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$id]);
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>beranda mahasiswa</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('https://www.its.ac.id/wp-content/uploads/2017/09/ini-bunderan-its-ya-1-1024x576.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					BERANDA MAHASISWA
				</span>

				<form 
				class="login100-form validate-form p-b-33 p-t-5" 
				method="POST"
				action="absen.php">
					<select class="form-control input100" name="matkul">
						<?php
							for ($i = 0; $row = $stmt->fetch(); $i++) {
						?>
						<option value=<?php echo $row['ID_Diambil'] ?> ><?php echo $row['nama_MK'] ?></option>
						<?php } ?>
					</select>
					<select class="input100 form-control m-t-32" name="status">
						<option value=1>Hadir</option>
						<option value=2>Sakit</option>
						<option value=3>Izin</option>
						<option value=4>Tanpa Keterangan</option>
					</select>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
                            simpan
						</button>
					</div>

				</form>
				<form action="absenmhs.php">
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Lihat Riwayat Kehadiran
						</button>
					</div>
				</form>
				<form method="POST" action="logout.php">
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Logout
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
