CREATE DATABASE  IF NOT EXISTS `final` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `final`;
-- MySQL dump 10.13  Distrib 8.0.16, for Linux (x86_64)
--
-- Host: localhost    Database: final
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `list_kelas`
--

DROP TABLE IF EXISTS `list_kelas`;
/*!50001 DROP VIEW IF EXISTS `list_kelas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `list_kelas` AS SELECT 
 1 AS `ID_KelasMK`,
 1 AS `jenis_KelasMK`,
 1 AS `ruang_KelasMK`,
 1 AS `FK_NIP_Dosen`,
 1 AS `dilalui_KelasMK`,
 1 AS `kode_MK`,
 1 AS `nama_MK`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `peserta_matkul`
--

DROP TABLE IF EXISTS `peserta_matkul`;
/*!50001 DROP VIEW IF EXISTS `peserta_matkul`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `peserta_matkul` AS SELECT 
 1 AS `ID_Diambil`,
 1 AS `NRP_Mhs`,
 1 AS `nama_Mhs`,
 1 AS `Nama_MK`,
 1 AS `NIP_Dosen`,
 1 AS `Kelas_MK`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `rekap_absensi`
--

DROP TABLE IF EXISTS `rekap_absensi`;
/*!50001 DROP VIEW IF EXISTS `rekap_absensi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `rekap_absensi` AS SELECT 
 1 AS `ID_Absensi`,
 1 AS `keterangan_Absensi`,
 1 AS `verif_Dsn`,
 1 AS `minggu_Absensi`,
 1 AS `NRP_Mhs`,
 1 AS `nama_Mhs`,
 1 AS `Nama_MK`,
 1 AS `Kelas_MK`,
 1 AS `ID_Diambil`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tb_absensi`
--

DROP TABLE IF EXISTS `tb_absensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_absensi` (
  `ID_Absensi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_Diambil` int(10) unsigned NOT NULL,
  `verifMhs_Absensi` int(1) NOT NULL DEFAULT '0',
  `verifDsn_Absensi` int(1) NOT NULL DEFAULT '0',
  `keterangan_Absensi` enum('hadir','sakit','izin','tanpa_keterangan') NOT NULL DEFAULT 'tanpa_keterangan',
  `minggu_Absensi` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_Absensi`),
  KEY `fk_tb_Absensi_tb_Diambil1_idx` (`FK_ID_Diambil`),
  CONSTRAINT `fk_tb_Absensi_tb_Diambil1` FOREIGN KEY (`FK_ID_Diambil`) REFERENCES `tb_diambil` (`ID_Diambil`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_absensi`
--

LOCK TABLES `tb_absensi` WRITE;
/*!40000 ALTER TABLE `tb_absensi` DISABLE KEYS */;
INSERT INTO `tb_absensi` VALUES (5,9,1,0,'hadir',1),(6,10,1,1,'hadir',1),(7,11,1,0,'hadir',1),(8,12,1,0,'hadir',1),(9,9,1,0,'hadir',2),(10,11,1,0,'izin',2),(11,10,1,0,'izin',2),(12,10,1,1,'hadir',3),(13,10,1,0,'izin',4),(14,10,1,0,'izin',5),(15,11,1,1,'sakit',3),(23,10,1,0,'izin',6),(24,12,1,0,'izin',2),(25,10,1,1,'izin',7),(26,12,1,1,'sakit',3),(27,9,1,1,'hadir',3),(28,12,1,1,'hadir',7),(29,9,1,0,'izin',4),(30,12,1,0,'hadir',8);
/*!40000 ALTER TABLE `tb_absensi` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tb_absensi_AFTER_INSERT` AFTER INSERT ON `tb_absensi` FOR EACH ROW BEGIN
    declare oldWeek int(2);
    declare kelasID int(11);
    set oldWeek = (select `kelas`.dilalui_KelasMK from tb_kelasmk as `kelas`
where `kelas`.ID_KelasMK in (
select `peserta`.Kelas_MK from peserta_matkul as `peserta` 
inner join tb_absensi as `absen` on `absen`.FK_ID_Diambil = `peserta`.ID_Diambil
where `absen`.FK_ID_Diambil = new.FK_ID_Diambil
));
    set kelasID = (select `diambil`.FK_ID_KelasMK from tb_diambil as `diambil` where `diambil`.ID_Diambil = new.FK_ID_Diambil);

    if (new.minggu_Absensi > oldWeek) then
        update tb_kelasmk set dilalui_KelasMK = new.minggu_Absensi where ID_KelasMK = kelasID;
    end if;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_diambil`
--

DROP TABLE IF EXISTS `tb_diambil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_diambil` (
  `ID_Diambil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FK_ID_KelasMK` int(11) NOT NULL,
  `FK_NRP_Mhs` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_Diambil`),
  UNIQUE KEY `ID_Diambil_UNIQUE` (`ID_Diambil`),
  KEY `fk_tb_Diambil_tb_KelasMK1_idx` (`FK_ID_KelasMK`),
  KEY `fk_tb_Diambil_tb_Mahasiswa1_idx` (`FK_NRP_Mhs`),
  CONSTRAINT `fk_tb_Diambil_tb_KelasMK1` FOREIGN KEY (`FK_ID_KelasMK`) REFERENCES `tb_kelasmk` (`ID_KelasMK`),
  CONSTRAINT `fk_tb_Diambil_tb_Mahasiswa1` FOREIGN KEY (`FK_NRP_Mhs`) REFERENCES `tb_mahasiswa` (`NRP_Mhs`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_diambil`
--

LOCK TABLES `tb_diambil` WRITE;
/*!40000 ALTER TABLE `tb_diambil` DISABLE KEYS */;
INSERT INTO `tb_diambil` VALUES (9,4,13),(10,6,15),(11,4,14),(12,6,13);
/*!40000 ALTER TABLE `tb_diambil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_dosen`
--

DROP TABLE IF EXISTS `tb_dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_dosen` (
  `NIP_Dosen` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_Dosen` varchar(100) NOT NULL,
  `username_Dosen` varchar(15) NOT NULL,
  `password_Dosen` varchar(32) NOT NULL,
  PRIMARY KEY (`NIP_Dosen`),
  UNIQUE KEY `NIK_UNIQUE` (`NIP_Dosen`),
  UNIQUE KEY `username_DS_UNIQUE` (`username_Dosen`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_dosen`
--

LOCK TABLES `tb_dosen` WRITE;
/*!40000 ALTER TABLE `tb_dosen` DISABLE KEYS */;
INSERT INTO `tb_dosen` VALUES (4,'dosen1','dosen1','123'),(5,'dosen2','dosen2','234'),(6,'dosen3','dosen3','345');
/*!40000 ALTER TABLE `tb_dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kelasmk`
--

DROP TABLE IF EXISTS `tb_kelasmk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_kelasmk` (
  `ID_KelasMK` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_KelasMK` varchar(1) NOT NULL,
  `ruang_KelasMK` varchar(6) NOT NULL,
  `FK_kode_MK` int(10) unsigned NOT NULL,
  `FK_NIP_Dosen` int(10) unsigned NOT NULL,
  `dilalui_KelasMK` int(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_KelasMK`),
  UNIQUE KEY `ID_KelasMK_UNIQUE` (`ID_KelasMK`),
  KEY `fk_tb_KelasMK_tb_Dosen1_idx` (`FK_NIP_Dosen`),
  KEY `fk_tb_KelasMK_tb_Mata_Kuliah1_idx` (`FK_kode_MK`),
  CONSTRAINT `fk_tb_KelasMK_tb_Dosen1` FOREIGN KEY (`FK_NIP_Dosen`) REFERENCES `tb_dosen` (`NIP_Dosen`),
  CONSTRAINT `fk_tb_KelasMK_tb_Mata_Kuliah1` FOREIGN KEY (`FK_kode_MK`) REFERENCES `tb_mata_kuliah` (`kode_MK`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kelasmk`
--

LOCK TABLES `tb_kelasmk` WRITE;
/*!40000 ALTER TABLE `tb_kelasmk` DISABLE KEYS */;
INSERT INTO `tb_kelasmk` VALUES (4,'A','A234',5,4,4),(5,'B','A235',5,5,1),(6,'A','A109',6,4,8);
/*!40000 ALTER TABLE `tb_kelasmk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logverif`
--

DROP TABLE IF EXISTS `tb_logverif`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_logverif` (
  `id_logVerif` int(11) NOT NULL AUTO_INCREMENT,
  `timeverfied_logVerif` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idDosen_logVerif` int(10) unsigned NOT NULL,
  `isian_logVerif` int(1) DEFAULT NULL,
  `idMhs_logVerif` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_logVerif`),
  KEY `fkDosen_logVerif_idx` (`timeverfied_logVerif`),
  KEY `fkDosen_logVerif` (`idDosen_logVerif`),
  KEY `fkMhs_logVerif_idx` (`idMhs_logVerif`),
  CONSTRAINT `fkDosen_logVerif` FOREIGN KEY (`idDosen_logVerif`) REFERENCES `tb_dosen` (`NIP_Dosen`) ON DELETE RESTRICT,
  CONSTRAINT `fkMhs_logVerif` FOREIGN KEY (`idMhs_logVerif`) REFERENCES `tb_mahasiswa` (`NRP_Mhs`) ON DELETE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logverif`
--

LOCK TABLES `tb_logverif` WRITE;
/*!40000 ALTER TABLE `tb_logverif` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_logverif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_mahasiswa`
--

DROP TABLE IF EXISTS `tb_mahasiswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_mahasiswa` (
  `NRP_Mhs` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_Mhs` varchar(15) NOT NULL,
  `password_Mhs` varchar(32) NOT NULL,
  `nama_Mhs` varchar(100) NOT NULL,
  `tb_Ortu_ID_Ortu` int(10) unsigned NOT NULL,
  PRIMARY KEY (`NRP_Mhs`),
  UNIQUE KEY `NRP_UNIQUE` (`NRP_Mhs`),
  UNIQUE KEY `username_Mhs_UNIQUE` (`username_Mhs`),
  KEY `fk_tb_Mahasiswa_tb_Ortu_idx` (`tb_Ortu_ID_Ortu`),
  CONSTRAINT `fk_tb_Mahasiswa_tb_Ortu` FOREIGN KEY (`tb_Ortu_ID_Ortu`) REFERENCES `tb_ortu` (`ID_Ortu`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_mahasiswa`
--

LOCK TABLES `tb_mahasiswa` WRITE;
/*!40000 ALTER TABLE `tb_mahasiswa` DISABLE KEYS */;
INSERT INTO `tb_mahasiswa` VALUES (13,'mhs1','123','mhs1',4),(14,'mhs2','234','mhs2',5),(15,'mhs3','345','mhs3',6);
/*!40000 ALTER TABLE `tb_mahasiswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_mata_kuliah`
--

DROP TABLE IF EXISTS `tb_mata_kuliah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_mata_kuliah` (
  `kode_MK` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_MK` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_MK`),
  UNIQUE KEY `id_UNIQUE` (`kode_MK`),
  UNIQUE KEY `nama_MK_UNIQUE` (`nama_MK`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_mata_kuliah`
--

LOCK TABLES `tb_mata_kuliah` WRITE;
/*!40000 ALTER TABLE `tb_mata_kuliah` DISABLE KEYS */;
INSERT INTO `tb_mata_kuliah` VALUES (7,'Etika'),(6,'Fisika Dasar 2'),(8,'Machine Learning'),(5,'PBO');
/*!40000 ALTER TABLE `tb_mata_kuliah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_ortu`
--

DROP TABLE IF EXISTS `tb_ortu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_ortu` (
  `ID_Ortu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_Ortu` varchar(30) NOT NULL,
  `password_Ortu` varchar(50) NOT NULL,
  `nama_Ortu` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_Ortu`),
  UNIQUE KEY `username_Ortu_UNIQUE` (`username_Ortu`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ortu`
--

LOCK TABLES `tb_ortu` WRITE;
/*!40000 ALTER TABLE `tb_ortu` DISABLE KEYS */;
INSERT INTO `tb_ortu` VALUES (4,'ortu1','123','ortuku'),(5,'ortu2','234','ortumu'),(6,'ortu3','345','ortukami');
/*!40000 ALTER TABLE `tb_ortu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_staff`
--

DROP TABLE IF EXISTS `tb_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tb_staff` (
  `NIP_Staff` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_Staff` varchar(15) NOT NULL,
  `password_Staff` varchar(32) NOT NULL,
  `nama_Staff` varchar(100) NOT NULL,
  PRIMARY KEY (`NIP_Staff`),
  UNIQUE KEY `NIP_ST_UNIQUE` (`NIP_Staff`),
  UNIQUE KEY `username_Staff_UNIQUE` (`username_Staff`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_staff`
--

LOCK TABLES `tb_staff` WRITE;
/*!40000 ALTER TABLE `tb_staff` DISABLE KEYS */;
INSERT INTO `tb_staff` VALUES (4,'staff1','123','staffku'),(5,'staff2','234','staffmu'),(6,'staff3','345','staffkami');
/*!40000 ALTER TABLE `tb_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'final'
--

--
-- Dumping routines for database 'final'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_Login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Login`(IN username varchar(15), IN pass varchar(32), out result varchar(10), out idResult int(10) )
BEGIN
	if exists (select 1 from tb_dosen where username_Dosen = username and password_Dosen = pass) then
    set result = "dosen";
    set idResult = (select NIP_Dosen from tb_dosen where username_Dosen = username and password_Dosen = pass);
    elseif exists (select 1 from tb_mahasiswa where username_Mhs = username and password_Mhs = pass) then
    set result = "mhs";
    set idResult = (select NRP_Mhs from tb_mahasiswa where username_Mhs = username and password_Mhs = pass);
    elseif exists (select 1 from tb_ortu where username_Ortu = username and password_Ortu = pass) then
    set result = "ortu";
    set idResult = (select ID_Ortu from tb_ortu where username_Ortu = username and password_Ortu = pass);
    elseif exists (select 1 from tb_staff where username_Staff = username and password_Staff = pass) then
    set result = "staff";
    set idResult = (select NIP_Staff from tb_staff where username_Staff = username and password_Staff = pass);
    else
    set result = "none";
    set idResult = 0;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Mhs_Absen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Mhs_Absen`(IN idAbsen int(10), IN statusAbsen int(1))
BEGIN
    declare currentWeek int(2);

    set currentWeek = (select minggu_Absensi from tb_absensi where FK_ID_Diambil = idAbsen order by ID_Absensi desc limit 1);

    if (currentWeek is not null) then
        insert into tb_absensi (FK_ID_Diambil, verifMhs_Absensi, keterangan_Absensi, minggu_Absensi) values (idAbsen, 1, statusAbsen, currentWeek+1);
    else
        insert into tb_absensi (FK_ID_Diambil, verifMhs_Absensi, keterangan_Absensi) values (idAbsen, 1, statusAbsen);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Verif` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Verif`(IN id INT(10), IN statusAbsen INT(1), IN minggu INT(2))
BEGIN
    if exists ( select 1 from tb_absensi where FK_ID_Diambil = id and minggu_Absensi = minggu ) then
    update tb_absensi set verifDsn_Absensi = 1, keterangan_Absensi = statusAbsen where FK_ID_Diambil = id and minggu_Absensi = minggu;
    else
    insert into tb_absensi (FK_ID_Diambil, verifMhs_Absensi, verifDsn_Absensi, keterangan_Absensi, minggu_Absensi) values (id,1,1,statusAbsen,minggu);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `list_kelas`
--

/*!50001 DROP VIEW IF EXISTS `list_kelas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `list_kelas` AS select `kelas`.`ID_KelasMK` AS `ID_KelasMK`,`kelas`.`jenis_KelasMK` AS `jenis_KelasMK`,`kelas`.`ruang_KelasMK` AS `ruang_KelasMK`,`kelas`.`FK_NIP_Dosen` AS `FK_NIP_Dosen`,`kelas`.`dilalui_KelasMK` AS `dilalui_KelasMK`,`mk`.`kode_MK` AS `kode_MK`,`mk`.`nama_MK` AS `nama_MK` from (`tb_kelasmk` `kelas` join `tb_mata_kuliah` `mk` on((`mk`.`kode_MK` = `kelas`.`FK_kode_MK`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `peserta_matkul`
--

/*!50001 DROP VIEW IF EXISTS `peserta_matkul`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peserta_matkul` AS select `ambil`.`ID_Diambil` AS `ID_Diambil`,`tb_mahasiswa`.`NRP_Mhs` AS `NRP_Mhs`,`tb_mahasiswa`.`nama_Mhs` AS `nama_Mhs`,`x`.`Nama_MK` AS `Nama_MK`,`x`.`NIP_Dosen` AS `NIP_Dosen`,`x`.`Kelas_MK` AS `Kelas_MK` from ((`tb_diambil` `ambil` join `tb_mahasiswa` on((`tb_mahasiswa`.`NRP_Mhs` = `ambil`.`FK_NRP_Mhs`))) join (select `mk`.`nama_MK` AS `Nama_MK`,`kelas`.`ID_KelasMK` AS `Kelas_MK`,`kelas`.`FK_NIP_Dosen` AS `NIP_Dosen` from (`tb_mata_kuliah` `mk` join `tb_kelasmk` `kelas`) where (`mk`.`kode_MK` = `kelas`.`FK_kode_MK`)) `x` on((`x`.`Kelas_MK` = `ambil`.`FK_ID_KelasMK`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rekap_absensi`
--

/*!50001 DROP VIEW IF EXISTS `rekap_absensi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rekap_absensi` AS select `absen`.`ID_Absensi` AS `ID_Absensi`,`absen`.`keterangan_Absensi` AS `keterangan_Absensi`,`absen`.`verifDsn_Absensi` AS `verif_Dsn`,`absen`.`minggu_Absensi` AS `minggu_Absensi`,`peserta_matkul`.`NRP_Mhs` AS `NRP_Mhs`,`peserta_matkul`.`nama_Mhs` AS `nama_Mhs`,`peserta_matkul`.`Nama_MK` AS `Nama_MK`,`peserta_matkul`.`Kelas_MK` AS `Kelas_MK`,`peserta_matkul`.`ID_Diambil` AS `ID_Diambil` from (`peserta_matkul` left join `tb_absensi` `absen` on((`absen`.`FK_ID_Diambil` = `peserta_matkul`.`ID_Diambil`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-06 21:33:37
