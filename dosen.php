<html>
  <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="desain.css">
    <style media="screen"></style>

  </head>
  <body>
<div class="wrapper fadeInDown">
  <div id="formContent">
    <h3>Verifikasi Absen</h3>
    <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Nama Mahasiswa</th>
      <th scope="col">NRP</th>
      <th scope="col">Nama Matkul</th>
      <th scope="col">Status Verifikasi</th>
      <th scope="col">Verifikasi</th>
      <th>Tidak Hadir</th>
    </tr>
  </thead>
  <tbody>

    <?php
      include('connect.php');
      $dosen = "dosen1";
      $mk = "fisika";
      $sql = 'select * from belum_verif where Nama_Dosen=? and Nama_MK=?';

      $stmt = $pdo->prepare($sql);
      $stmt->execute([$dosen,$mk]);
      for ($i = 0; $row = $stmt->fetch(); $i++) {
        ?>

      <tr>
        <td><?php echo $row['nama_Mhs'] ?></td>
        <td><?php echo $row['NRP_Mhs'] ?></td>
        <td><?php echo $row['Nama_MK'] ?></td>

          <?php 
            if ($row['hadir_Absensi'] == 0) {
              ?>
              <td><?php echo "Belum Verif" ?></td>
              <td>
                <button 
                  type="button" 
                  class="btn btn-primary" 
                  onclick="toggleVerif(<?php echo $row['NRP_Mhs'] ?>,<?php echo $row['ID_Diambil'] ?>, <?php echo $row['NIP_Dosen']?>,<?php echo $row['ID_Absensi'] ?>)">
                  Verif
                </button>
              </td>

              <td>
                <button 
                  type="button" 
                  class="btn btn-danger" 
                  data-toggle="modal" data-target="#exampleModal">
                  Tidak Hadir
                </button>
              </td>
            <?php } else { ?>
              <td><?php echo "Sudah Verif" ?></td>
              <td>
                <button 
                  type="button" 
                  class="btn btn-danger" 
                  onclick="toggleVerif(<?php echo $row['NRP_Mhs'] ?>,<?php echo $row['ID_Diambil'] ?>, <?php echo $row['NIP_Dosen']?>,<?php echo $row['ID_Absensi'] ?>)">
                  Unverif
                </button>
              </td>
              <td>-</td>
            <?php } ?>
      </tr>

      <?php } ?>

 </tbody>
</table>
  </div>
</div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Keterangan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="keterangan_form" onsubmit="tidakHadir()">
                <input type="text" id="ket" name="ket" placeholder="Keterangan" />
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cancel</button>
              <button type="button" class="btn btn-primary" onclick="tidakHadir()">Submit</button>
            </div>
          </div>
        </div>
      </div>
      

      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script>
        function toggleVerif (nrpMhs, idDiambil, nipDosen,idAbsen) {
          if (idAbsen === undefined) {
            idAbsen = '';
          }
          $.ajax({
            url: "verif.php",
            type: "post",
            dataType: 'json',
            data: {id: idAbsen, nrp: nrpMhs, diambil: idDiambil, nip: nipDosen}
          }).done(function(msg) {
            window.location.reload();
          }) 
        }

        function tidakHadir(idUser) {
          var ket = $("#ket").val();
        }
      </script>

</body>

